<?php

namespace Login\Controller;

use Base\Utility\Mail;
use Base\Utility\UserPassword;
use Login\Form\RecoverForm;
use Login\Validator\UserExistsValidator;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Authentication\Result;
use Zend\Uri\Uri;
use Login\Form\LoginForm;
use Login\Form\ForgotForm;
use Login\Entity\Usuario;

/**
 * This controller is responsible for letting the user to log in and log out.
 */
class AuthController extends AbstractActionController
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager 
     */
    private $entityManager;
    
    /**
     * Auth manager.
     * @var Login\Service\AuthManager
     */
    private $authManager;
    
    /**
     * Auth service.
     * @var \Zend\Authentication\AuthenticationService
     */
    private $authService;
    
    /**
     * User manager.
     * @var Login\Service\UserManager
     */
    private $userManager;

    private $config;
    /**
     * Constructor.
     */
    public function __construct($entityManager, $authManager, $authService, $userManager, $config)
    {
        $this->entityManager = $entityManager;
        $this->authManager = $authManager;
        $this->authService = $authService;
        $this->userManager = $userManager;
        $this->config = $config;
    }
    
    /**
     * Authenticates user given email address and password credentials.     
     */
    public function loginAction()
    {
        // Retrieve the redirect URL (if passed). We will redirect the user to this
        // URL after successfull login.
        $redirectUrl = (string)$this->params()->fromQuery('redirectUrl', '');
        if (strlen($redirectUrl)>2048) {
            throw new \Exception("Too long redirectUrl argument passed");
        }
        
        // Check if we do not have users in database at all. If so, create 
        // the 'Admin' user.
//        $this->userManager->createAdminUserIfNotExists();
        
        // Create login form
        $form = new LoginForm(); 
        $form->get('redirect_url')->setValue($redirectUrl);
        
        // Store login status.
        $isLoginError = false;
        
        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            
            // Fill in the form with POST data
            $data = $this->params()->fromPost();            
            
            $form->setData($data);
            // Validate form
            if($form->isValid()) {

                // Get filtered and validated data
                $data = $form->getData();

                // Perform login attempt.
                $result = $this->authManager->login($data['email'], 
                        $data['senha'], $data['remember_me']);
                // Check result.
                if ($result->getCode() == Result::SUCCESS) {

                    // Get redirect URL.
                    $redirectUrl = $this->params()->fromPost('redirect_url', '');
                    
                    if (!empty($redirectUrl)) {
                        // The below check is to prevent possible redirect attack 
                        // (if someone tries to redirect user to another domain).
                        $uri = new Uri($redirectUrl);
                        if (!$uri->isValid() || $uri->getHost()!=null)
                            throw new \Exception('Incorrect redirect URL: ' . $redirectUrl);
                    }

                    // If redirect URL is provided, redirect the user to that URL;
                    // otherwise redirect to Home page.
                    $this->flashMessenger()->addSuccessMessage(array('Login efetuado com sucesso', 'Seja bem-vindo!'));
                    if(empty($redirectUrl)) {
                        return $this->redirect()->toRoute('admin');
                    } else {
                        $this->redirect()->toUrl($redirectUrl);
                    }
                } else {
                    $this->flashMessenger()->addErrorMessage(array($result->getMessages()[0], 'Opss!'));
                }
            } else {
                $this->flashMessenger()->addErrorMessage(array('Dados incorretos', 'Opss!'));

            }           
        } 
        
        return new ViewModel([
            'form' => $form,
            'isLoginError' => $isLoginError,
            'redirectUrl' => $redirectUrl
        ]);
    }

    /**
     * Authenticates user given email address and password credentials from external apps.
     */
    public function loginexternalAction()
    {
        header('Access-Control-Allow-Origin: *');
        $retorno = array('result'=>'failed','Nenhum dado recebdio');
        // Store login status.
        $isLoginError = false;

        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {

            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            // Perform login attempt.
            $result = $this->authManager->login($data['email'],
                    $data['senha'], $data['rememberme']);
            // Check result.
            if ($result->getCode() == Result::SUCCESS) {

                // otherwise redirect to Home page.
                $retorno = array('result'=>'success','message'=>'Login efetuado com sucesso', 'remember' => $data['rememberme'],  'usuario'=> $result->getIdentity()->toArray());

            } else {
                $retorno = array('result'=>'failed', 'message' => $result->getMessages()[0]);
            }

        }

        return new JsonModel($retorno);
    }
    
    /**
     * The "logout" action performs logout operation.
     */
    public function logoutAction() 
    {
        header('Access-Control-Allow-Origin: *');
        $external = $this->params()->fromQuery('external', false);
        $this->authManager->logout();

        if($external){
            $retorno = array('result'=>'success','message'=>'Logout efetuado com sucesso');
            return new JsonModel($retorno);
        }

        $this->flashMessenger()->addSuccessMessage(array('Logout efetuado com sucesso'));

        return $this->redirect()->toRoute('login');
    }

    /**
     * The "forgot" action performs forgot password.
     */
    public function forgotAction()
    {
        $form = new ForgotForm();

        // Store forgot status.
        $isForgotError = false;
        $isForgotSuccess = false;
        // Check if user has submitted the form
        if ($this->getRequest()->isPost()) {
            $data = $this->params()->fromPost();

            $form->setData($data);
            // Validate form
            if($form->isValid()) {
                $usuario = $this->entityManager->getRepository(Usuario::class)->findOneByEmail($this->params()->fromPost('email', ''));

                if (!is_null($usuario)) {
                    $userPassword = new UserPassword();
                    $data = new \DateTime();
                    $token = $userPassword->create($data->format('d-m-Y-H-i-s'));

                    $usuario->setTokenSenha($token);
                    $this->entityManager->persist($usuario);
                    $this->entityManager->flush();

                    $email = new Mail(array('entityManager'=>$this->entityManager));
                    $url = $this->url()->fromRoute('recover', array(), array('query' => array('id'=>$token), 'force_canonical' => true));
                    $mensagem = array('template'=>'forgot.phtml', 'params'=> array('url'=>$url));

                    if($email->sendMail($email->getEmail(), $email->getEmail(), $usuario->getEmail(), $usuario->getNome(), false, false, 'Recuperar senha', $mensagem, false, $this->config)){

                        $this->flashMessenger()->addSuccessMessage(array('Verifique seu e-mail.'));
                        return $this->redirect()->toRoute('login');
                    }else{
                        $this->flashMessenger()->addWarningMessage(array('Tente novamente mais tarde!', 'Ocorreu um erro!'));
                    }
                }else{
                    $this->flashMessenger()->addErrorMessage(array('E-mail não encontrado.', 'Opss!'));

                }
            }else{
                $this->flashMessenger()->addErrorMessage(array('Dados informados inválidos.', 'Opss!'));
            }
        }

        return new ViewModel(array('form'=>$form));
    }

    /**
     * The "recover" action performs recover password.
     */
    public function recoverAction()
    {
        $form = new RecoverForm();

        // Store recover status.
        $isRecoverError = false;
        $isRecoverSuccess = false;

        $usuario = $this->entityManager->getRepository(Usuario::class)->findOneByTokenSenha($this->params()->fromQuery('id', 0));
        if(!is_null($usuario)){
            if ($this->getRequest()->isPost()) {
                $data = $this->params()->fromPost();

                $form->setData($data);
                // Validate form
                if($form->isValid()) {
                    $userPassword = new UserPassword();
                    $senha = $userPassword->create($data['senha']);

                    $usuario->setSenha($senha);
                    $usuario->setTokenSenha('');

                    $this->entityManager->persist($usuario);
                    $this->entityManager->flush();

                    $this->flashMessenger()->addSuccessMessage(array('Sua senha foi alterada com sucesso!'));

                    $email = new Mail(array('entityManager'=>$this->entityManager, 'baseUrl'=>$baseUrl));
                    $url = $this->url()->fromRoute('home', array(), array('force_canonical' => true));
                    $mensagem = array('template'=>'recover.phtml', 'params'=> array('url'=>$url));
                    $email->sendMail($email->getConfiguracao()->getMailEmail(), $email->getConfiguracao()->getMailEmail(), $usuario->getEmail(), $usuario->geNome(), false, false, 'Senha alterada', $mensagem, false, $this->config);
                    return $this->redirect()->toRoute('login');

                }else{
                    $this->flashMessenger()->addErrorMessage(array('Dados informados inválidos!'));
                    $this->flashMessenger()->addErrorMessage(array('A senha deve ter no mínimo 6 caracteres!'));
                }
            }
        }else{
            $this->flashMessenger()->addErrorMessage(array('A senha deve ter no mínimo 6 caracteres!', 'Opss'));
        }

        return new ViewModel(array('form'=>$form,
            'isRecoverError' => $isRecoverError,
            'isRecoverSuccess' => $isRecoverSuccess));
    }
}
