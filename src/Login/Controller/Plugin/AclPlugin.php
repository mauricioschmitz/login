<?php
/**
 */

namespace Login\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\View\Helper\ViewModel;
use Zend\Authentication\Storage\Session as SessionStorage;

class AclPlugin  extends AbstractPlugin
{

    /**
     * @var Login\Service\Acl
     */
    private $acl;

    /**
     * @var Zend\Session\SessionManager;
     */
    private $sessionManager;

    /**
     * @var Zend\Authentication\AuthenticationService
     */
    private $authenticationService;

    /**
     * Constructs the service.
     */
    public function __construct($acl, $sessionManager, $authenticationService)
    {
        $this->acl = $acl;
        $this->sessionManager = $sessionManager;
        $this->authenticationService = $authenticationService;
    }
    /**
     * Invoke Helper
     * @return boolean
     */
    public function __invoke($module = null, $controller = 'index', $action = 'index') {

        $resource = ucfirst($module).'\\'.ucfirst($controller);
        $permission = $action;
        if($authenticationService->hasIdentity()){
            $loggedUser = $authenticationService->getIdentity();
            $roles = $loggedUser->getPerfis();
            if ($this->acl->isAccessAllowed($roles, $resource, $permission)) {
                return true;
            }
        }
        return false;
    }

}
