<?php
namespace Login\Controller\Plugin\Factory;

use Interop\Container\ContainerInterface;
use Login\Controller\Plugin\AclPlugin;
use Login\Service\Acl;
use Login\View\Helper\AclHelper;
use Zend\Session\SessionManager;

/**
 * This is the factory class for UserManager service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class AclPluginFactory
{
    /**
     * This method creates the UserManager service and returns its instance. 
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $acl = $container->get('Login\Service\Acl');
        $sessionManager = $container->get(SessionManager::class);
        $authenticationService = $container->get('Zend\Authentication\AuthenticationService');

        return new AclPlugin($acl, $sessionManager, $authenticationService);
    }
}
