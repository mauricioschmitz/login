<?php
/**
 */

namespace Login\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\Storage\Session as SessionStorage;


class AclHelper  extends \Zend\View\Helper\AbstractHelper
{

    /**
     * @var Login\Service\Acl
     */
    private $acl;

    /**
     * @var Zend\Session\SessionManager;
     */
    private $sessionManager;

    /**
     * @var Zend\Authentication\AuthenticationService
     */
    private $authenticationService;

    /**
     * Constructs the service.
     */
    public function __construct($acl, $sessionManager, $authenticationService)
    {
        $this->acl = $acl;
        $this->sessionManager = $sessionManager;
        $this->authenticationService = $authenticationService;
    }
    /**
     * Invoke Helper
     * @return boolean
     */
    public function __invoke($module = null, $controller = 'index', $action = 'index') {
        $resource = ucfirst($module).'\\'.ucfirst($controller);
        $permission = $action;
        $loggedUser = $this->authenticationService->getIdentity();
        if(!is_null($loggedUser)) {
            $roles = $loggedUser->getPerfis();
            if ($this->acl->isAccessAllowed($roles, $resource, $permission)) {
                return true;
            }
        }
        return false;
    }

}
