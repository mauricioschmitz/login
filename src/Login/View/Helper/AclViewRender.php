<?php
/**
 */

namespace Login\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\Storage\Session as SessionStorage;


class AclViewRender  extends \Zend\View\Helper\AbstractHelper
{

    /**
     * @var Login\Service\Acl
     */
    private $acl;

    /**
     * @var Zend\Session\SessionManager;
     */
    private $sessionManager;

    /**
     * @var Zend\Authentication\AuthenticationService
     */
    private $authenticationService;

    /**
     * Constructs the service.
     */
    public function __construct($acl, $sessionManager, $authenticationService)
    {
        $this->acl = $acl;
        $this->sessionManager = $sessionManager;
        $this->authenticationService = $authenticationService;
    }
    /**
     * Invoke Helper
     * @return boolean
     */
    public function __invoke($route = 'home', $module = null, $controller = 'index', $action = 'index', $id = null, $query=array(), $template = '', $variables = array()) {
        $resource = ucfirst($module).'\\'.ucfirst($controller);
        $permission = $action;

        $loggedUser = $this->authenticationService->getIdentity();

        if(!is_null($loggedUser)) {
            $roles = $loggedUser->getPerfis();
            if ($this->acl->isAccessAllowed($roles, $resource, $permission)) {
                $params = array('modulo' => $module, 'controller' => $controller, 'action' => ($action == 'index' ? null : $action), 'id' => $id);
                $variables['url'] = $this->view->url($route, $params, array('query' => $query));

                //Trata a permissão
                return $this->replaces($template, $variables);

            }
        }
        return '';
    }

    private function replaces($template = '', $variables = array()){
        foreach ($variables as $key=>$variable){
            $template = str_replace('%'.$key.'%', $variable, $template);
        }
        return  $template;
    }
}
