<?php
namespace Login\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

/**
 * This form is used to collect user's login, password and 'Remember Me' flag.
 */
class RecoverForm extends AbstractForm
{
    /**
     * Constructor.     
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('login-recover');
     
        // Set POST method for this form
        $this->setAttribute('class', 'm-t validate');
        $this->setAttribute('role', 'form');

        $this->addElements();
        $this->addInputFilter();

    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        // Add "password" field
        $this->add([
            'type'  => 'password',
            'name' => 'senha',
            'attributes' => [
                'id' => 'senha',
                'class' => 'form-control text-muted',
                'required' => 'true',
                'placeholder' => 'Senha',
                'title' => 'Informe sua senha',
                'data-rule-minlength' => 6,
                'data-msg-minlength' => 'Sua senha deve conter no mínimo 6 caracteres!'
            ],
            'options' => [
                'label' => 'Senha',
            ],
        ]);

        // Add "confirm password" field
        $this->add([
            'type'  => 'password',
            'name' => 'confirmar_senha',
            'attributes' => [
                'class' => 'form-control text-muted',
                'required' => 'true',
                'placeholder' => 'Confirmar senha',
                'title' => 'Confirme sua senha',
                'data-rule-equalTo' => '#senha',
                'data-rule-minlength' => 6,
                'data-msg-minlength' => 'Sua senha deve conter no mínimo 6 caracteres!'

            ],
            'options' => [
                'label' => 'Confirmar senha',
            ],
        ]);
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Enviar',
                'class' => 'btn btn-success btn-block m-b ladda-button'
            ),
            'options' => array(
                'label' =>'Enviar',

            )
        ));
    }
    
    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter() 
    {
        // Create main input filter
        $inputFilter = new InputFilter();        
        $this->setInputFilter($inputFilter);

        // Add input for "password" field
        $inputFilter->add([
            'name'     => 'senha',
            'required' => true,
            'filters'  => [
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'min' => 6,
                        'max' => 64
                    ],
                ],
            ],
        ]);

        // Add input for "confirm_new_password" field
        $inputFilter->add([
            'name'     => 'confirmar_senha',
            'required' => true,
            'filters'  => [
            ],
            'validators' => [
                [
                    'name'    => 'Identical',
                    'options' => [
                        'token' => 'senha',
                    ],
                ],
            ],
        ]);

    }        
}

