<?php
namespace Login\Form;

use Base\Form\AbstractForm;
use Zend\Form\Form;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilter;

/**
 * This form is used to collect user's login, password and 'Remember Me' flag.
 */
class ForgotForm extends AbstractForm
{
    /**
     * Constructor.     
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('login-forgot');
     
        // Set POST method for this form
        $this->setAttribute('class', 'm-t validate');
        $this->setAttribute('role', 'form');

        $this->addElements();
        $this->addInputFilter();

    }
    
    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements() 
    {
        // Add "email" field
        $this->add([            
            'type'  => 'email',
            'name' => 'email',
            'attributes' => [
                'class' => 'form-control text-muted',
                'required' => 'true',
                'placeholder' => 'E-mail',
                'title' => 'Informe seu e-mail'
            ],
            'options' => [
                'label' => 'E-mail',
            ],
        ]);
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Enviar',
                'class' => 'btn btn-success btn-block m-b ladda-button'
            ),
            'options' => array(
                'label' =>'Enviar',

            )
        ));
    }
    
    /**
     * This method creates input filter (used for form filtering/validation).
     */
    private function addInputFilter() 
    {
        // Create main input filter
        $inputFilter = new InputFilter();        
        $this->setInputFilter($inputFilter);
                
        // Add input for "email" field
        $inputFilter->add([
            'name'     => 'email',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'EmailAddress',
                    'options' => [
                        'allow' => \Zend\Validator\Hostname::ALLOW_DNS,
                        'useMxCheck' => false,
                    ],
                ],
            ],
        ]);

    }        
}

