<?php

namespace Login\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class UsuarioPermissaoRepository extends EntityRepository  {


    public function getPermissaoControllerAction($usuario) {
        $qb = $this->_em->createQueryBuilder();
        $qb->select(array("a.nome as permissao, u.negar"))//

            ->from('Login\Entity\UsuarioPermissao', 'u')

            ->leftJoin('Login\Entity\ControllerAction', 'ca', Join::WITH, 'ca = u.controllerAction')
            ->leftJoin('Login\Entity\Action', 'a', Join::WITH, 'a = ca.action')
            ->leftJoin('Login\Entity\Controller', 'c', Join::WITH, 'c = ca.controller')
            ->leftJoin('Login\Entity\Module', 'm', Join::WITH, 'm = c.module')
            ->where('u.usuario = ?1')
            ->andWhere('u.ativo = 1 AND ca.ativo = 1 AND a.ativo = 1 AND c.ativo = 1 AND m.ativo = 1')
            ->setParameter(1, $usuario);
        $result = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

        return $result;
       
    }

}
