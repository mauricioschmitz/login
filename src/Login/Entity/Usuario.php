<?php

namespace Login\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;

/**
 * Usuario
 *
 * @ORM\Table(name="usuario", uniqueConstraints={@ORM\UniqueConstraint(name="token_senha_UNIQUE", columns={"token_senha"})}, indexes={@ORM\Index(name="fk_Usuario$usuario$Usuario_id_idx", columns={"Usuario_id"})})
 * @ORM\Entity(repositoryClass="Base\Entity\DefaultRepository")
 */
class Usuario extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255, nullable=false)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="senha", type="string", length=255, nullable=false)
     */
    private $senha;

    /**
     * @var string
     *
     * @ORM\Column(name="token_senha", type="string", length=255, nullable=true)
     */
    private $tokenSenha;

    /**
     * @var boolean
     *
     * @ORM\Column(name="erros_senha", type="boolean", nullable=false)
     */
    private $errosSenha = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="novidades", type="boolean", nullable=false)
     */
    private $novidades = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="confirmado", type="boolean", nullable=false)
     */
    private $confirmado = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="boolean", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_modificacao", type="datetime", nullable=true)
     */
    private $dataModificacao;

    /**
     * @ORM\ManyToMany(targetEntity="Login\Entity\Perfil", fetch="EAGER")
     * @ORM\JoinTable(name="usuario_perfil",
     *      joinColumns={@ORM\JoinColumn(name="usuario_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="perfil_id", referencedColumnName="id")}
     *      )
     */
    private $perfis;

    public function __construct(array $options = array()) {
        parent::__construct($options);
        $this->perfis = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Usuario
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Usuario
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set senha
     *
     * @param string $senha
     *
     * @return Usuario
     */
    public function setSenha($senha)
    {
        $this->senha = $senha;

        return $this;
    }

    /**
     * Get senha
     *
     * @return string
     */
    public function getSenha()
    {
        return $this->senha;
    }

    /**
     * Set tokenSenha
     *
     * @param string $tokenSenha
     *
     * @return Usuario
     */
    public function setTokenSenha($tokenSenha)
    {
        $this->tokenSenha = $tokenSenha;

        return $this;
    }

    /**
     * Get tokenSenha
     *
     * @return string
     */
    public function getTokenSenha()
    {
        return $this->tokenSenha;
    }

    /**
     * Set errosSenha
     *
     * @param boolean $errosSenha
     *
     * @return Usuario
     */
    public function setErrosSenha($errosSenha)
    {
        $this->errosSenha = $errosSenha;

        return $this;
    }

    /**
     * Get errosSenha
     *
     * @return boolean
     */
    public function getErrosSenha()
    {
        return $this->errosSenha;
    }

    /**
     * Set novidades
     *
     * @param boolean $novidades
     *
     * @return Usuario
     */
    public function setNovidades($novidades)
    {
        $this->novidades = $novidades;

        return $this;
    }

    /**
     * Get novidades
     *
     * @return boolean
     */
    public function getNovidades()
    {
        return $this->novidades;
    }

    /**
     * Set confirmado
     *
     * @param boolean $confirmado
     *
     * @return Usuario
     */
    public function setConfirmado($confirmado)
    {
        $this->confirmado = $confirmado;

        return $this;
    }

    /**
     * Get confirmado
     *
     * @return boolean
     */
    public function getConfirmado()
    {
        return $this->confirmado;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     *
     * @return Usuario
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return Usuario
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return Usuario
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }

    /**
     * Get perfis
     *
     * @return \Doctrine\Common\Collections\ArrayCollection;
     */

    public function getPerfis()
    {
        return $this->perfis;
    }
}
