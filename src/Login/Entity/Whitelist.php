<?php

namespace Login\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;

/**
 * Whitelist
 *
 * @ORM\Table(name="whitelist", uniqueConstraints={@ORM\UniqueConstraint(name="controller_action_id_UNIQUE", columns={"controller_action_id"})})
 * @ORM\Entity(repositoryClass="Base\Entity\DefaultRepository")
 */
class Whitelist extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="boolean", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_modificacao", type="datetime", nullable=true)
     */
    private $dataModificacao;

    /**
     * @var \Login\Entity\ControllerAction
     *
     * @ORM\ManyToOne(targetEntity="Login\Entity\ControllerAction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="controller_action_id", referencedColumnName="id")
     * })
     */
    private $controllerAction;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     *
     * @return Whitelist
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return Whitelist
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return Whitelist
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }

    /**
     * Set controllerAction
     *
     * @param \Login\Entity\ControllerAction $controllerAction
     *
     * @return Whitelist
     */
    public function setControllerAction(\Login\Entity\ControllerAction $controllerAction = null)
    {
        $this->controllerAction = $controllerAction;

        return $this;
    }

    /**
     * Get controllerAction
     *
     * @return \Login\Entity\ControllerAction
     */
    public function getControllerAction()
    {
        return $this->controllerAction;
    }
}
