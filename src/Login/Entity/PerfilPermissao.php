<?php

namespace Login\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;

/**
 * PerfilPermissao
 *
 * @ORM\Table(name="perfil_permissao", uniqueConstraints={@ORM\UniqueConstraint(name="perfil_permissao_UNIQUE", columns={"perfil_id", "controller_action_id"})}, indexes={@ORM\Index(name="fk_controller_action$perfil_permissao$controller_action_id_idx", columns={"controller_action_id"}), @ORM\Index(name="fk_perfil$perfil_permissao$perfil_id_idx", columns={"perfil_id"})})
 * @ORM\Entity(repositoryClass="Base\Entity\DefaultRepository")
 */
class PerfilPermissao extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="boolean", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_modificacao", type="datetime", nullable=true)
     */
    private $dataModificacao;

    /**
     * @var \Login\Entity\ControllerAction
     *
     * @ORM\ManyToOne(targetEntity="Login\Entity\ControllerAction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="controller_action_id", referencedColumnName="id")
     * })
     */
    private $controllerAction;

    /**
     * @var \Login\Entity\Perfil
     *
     * @ORM\ManyToOne(targetEntity="Login\Entity\Perfil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="perfil_id", referencedColumnName="id")
     * })
     */
    private $perfil;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     *
     * @return PerfilPermissao
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return PerfilPermissao
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return PerfilPermissao
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }

    /**
     * Set controllerAction
     *
     * @param \Login\Entity\ControllerAction $controllerAction
     *
     * @return PerfilPermissao
     */
    public function setControllerAction(\Login\Entity\ControllerAction $controllerAction = null)
    {
        $this->controllerAction = $controllerAction;

        return $this;
    }

    /**
     * Get controllerAction
     *
     * @return \Login\Entity\ControllerAction
     */
    public function getControllerAction()
    {
        return $this->controllerAction;
    }

    /**
     * Set perfil
     *
     * @param \Login\Entity\Perfil $perfil
     *
     * @return PerfilPermissao
     */
    public function setPerfil(\Login\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil
     *
     * @return \Login\Entity\Perfil
     */
    public function getPerfil()
    {
        return $this->perfil;
    }
}
