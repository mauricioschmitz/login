<?php

namespace Login\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;

/**
 * Controller
 *
 * @ORM\Table(name="controller", uniqueConstraints={@ORM\UniqueConstraint(name="controller_UNIQUE", columns={"module_id", "nome"})}, indexes={@ORM\Index(name="fk_module$controller$module_id_idx", columns={"module_id"})})
 * @ORM\Entity(repositoryClass="Base\Entity\DefaultRepository")
 */
class Controller extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=45, nullable=false)
     */
    private $nome;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="boolean", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_modificacao", type="datetime", nullable=true)
     */
    private $dataModificacao;

    /**
     * @var \Login\Entity\Module
     *
     * @ORM\ManyToOne(targetEntity="Login\Entity\Module", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="module_id", referencedColumnName="id")
     * })
     */
    private $module;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Controller
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     *
     * @return Controller
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return Controller
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return Controller
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }

    /**
     * Set module
     *
     * @param \Login\Entity\Module $module
     *
     * @return Controller
     */
    public function setModule(\Login\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return \Login\Entity\Module
     */
    public function getModule()
    {
        return $this->module;
    }
}
