<?php

namespace Login\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;

/**
 * ControllerActionFilho
 *
 * @ORM\Table(name="controller_action_filho", uniqueConstraints={@ORM\UniqueConstraint(name="controller_action_filho_UNIQUE", columns={"controller_id", "action_id", "controller_action_id"})}, indexes={@ORM\Index(name="fk_action$controller_action_filho$action_id_idx", columns={"action_id"}), @ORM\Index(name="fk_con_act$controller_action_filho$controller_action_id_idx", columns={"controller_action_id"}), @ORM\Index(name="fk_controller$controller_action_filho$controller_id_idx", columns={"controller_id"})})
 * @ORM\Entity(repositoryClass="Base\Entity\DefaultRepository")
 */
class ControllerActionFilho extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="boolean", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_modificacao", type="datetime", nullable=true)
     */
    private $dataModificacao;

    /**
     * @var \Login\Entity\Action
     *
     * @ORM\ManyToOne(targetEntity="Login\Entity\Action")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     * })
     */
    private $action;

    /**
     * @var \Login\Entity\ControllerAction
     *
     * @ORM\ManyToOne(targetEntity="Login\Entity\ControllerAction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="controller_action_id", referencedColumnName="id")
     * })
     */
    private $controllerAction;

    /**
     * @var \Login\Entity\Controller
     *
     * @ORM\ManyToOne(targetEntity="Login\Entity\Controller")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="controller_id", referencedColumnName="id")
     * })
     */
    private $controller;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     *
     * @return ControllerActionFilho
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return ControllerActionFilho
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return ControllerActionFilho
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }

    /**
     * Set action
     *
     * @param \Login\Entity\Action $action
     *
     * @return ControllerActionFilho
     */
    public function setAction(\Login\Entity\Action $action = null)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return \Login\Entity\Action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set controllerAction
     *
     * @param \Login\Entity\ControllerAction $controllerAction
     *
     * @return ControllerActionFilho
     */
    public function setControllerAction(\Login\Entity\ControllerAction $controllerAction = null)
    {
        $this->controllerAction = $controllerAction;

        return $this;
    }

    /**
     * Get controllerAction
     *
     * @return \Login\Entity\ControllerAction
     */
    public function getControllerAction()
    {
        return $this->controllerAction;
    }

    /**
     * Set controller
     *
     * @param \Login\Entity\Controller $controller
     *
     * @return ControllerActionFilho
     */
    public function setController(\Login\Entity\Controller $controller = null)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return \Login\Entity\Controller
     */
    public function getController()
    {
        return $this->controller;
    }
}
