<?php

namespace Login\Entity;

use Doctrine\ORM\Mapping as ORM;
use Base\Entity\AbstractEntity;

/**
 * UsuarioPermissao
 *
 * @ORM\Table(name="usuario_permissao", uniqueConstraints={@ORM\UniqueConstraint(name="usuario_permissao_UNIQUE", columns={"usuario_id", "controller_action_id"})}, indexes={@ORM\Index(name="fk_controller_action$ident_perm$controller_action_id_idx", columns={"controller_action_id"}), @ORM\Index(name="fk_usuario$usuario_permissao$usuario_id_idx", columns={"usuario_id"})})
 * @ORM\Entity(repositoryClass="Login\Entity\UsuarioPermissaoRepository")
 */
class UsuarioPermissao extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="negar", type="boolean", nullable=false)
     */
    private $negar = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="ativo", type="boolean", nullable=false)
     */
    private $ativo = '1';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_criacao", type="datetime", nullable=false)
     */
    private $dataCriacao;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_modificacao", type="datetime", nullable=true)
     */
    private $dataModificacao;

    /**
     * @var \Login\Entity\ControllerAction
     *
     * @ORM\ManyToOne(targetEntity="Login\Entity\ControllerAction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="controller_action_id", referencedColumnName="id")
     * })
     */
    private $controllerAction;

    /**
     * @var \Login\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="Login\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set negar
     *
     * @param boolean $negar
     *
     * @return UsuarioPermissao
     */
    public function setNegar($negar)
    {
        $this->negar = $negar;

        return $this;
    }

    /**
     * Get negar
     *
     * @return boolean
     */
    public function getNegar()
    {
        return $this->negar;
    }

    /**
     * Set ativo
     *
     * @param boolean $ativo
     *
     * @return UsuarioPermissao
     */
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set dataCriacao
     *
     * @param \DateTime $dataCriacao
     *
     * @return UsuarioPermissao
     */
    public function setDataCriacao($dataCriacao)
    {
        $this->dataCriacao = $dataCriacao;

        return $this;
    }

    /**
     * Get dataCriacao
     *
     * @return \DateTime
     */
    public function getDataCriacao()
    {
        return $this->dataCriacao;
    }

    /**
     * Set dataModificacao
     *
     * @param \DateTime $dataModificacao
     *
     * @return UsuarioPermissao
     */
    public function setDataModificacao($dataModificacao)
    {
        $this->dataModificacao = $dataModificacao;

        return $this;
    }

    /**
     * Get dataModificacao
     *
     * @return \DateTime
     */
    public function getDataModificacao()
    {
        return $this->dataModificacao;
    }

    /**
     * Set controllerAction
     *
     * @param \Login\Entity\ControllerAction $controllerAction
     *
     * @return UsuarioPermissao
     */
    public function setControllerAction(\Login\Entity\ControllerAction $controllerAction = null)
    {
        $this->controllerAction = $controllerAction;

        return $this;
    }

    /**
     * Get controllerAction
     *
     * @return \Login\Entity\ControllerAction
     */
    public function getControllerAction()
    {
        return $this->controllerAction;
    }

    /**
     * Set usuario
     *
     * @param \Login\Entity\Usuario $usuario
     *
     * @return UsuarioPermissao
     */
    public function setUsuario(\Login\Entity\Usuario $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \Login\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }
}
