<?php
namespace Login\Service;

class Whitelist
{

    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Zend Session
     * @var Zend\Authentication\Storage\Session
     */
    private $storage;

    /**
     * Constructs the service.
     */
    public function __construct($entityManager, $storage)
    {
        $this->entityManager = $entityManager;
        $this->storage = $storage;
    }

    public function getWhitelist()
    {
        if($this->storage->isEmpty()) {
            $whitelistList = $this->entityManager->getRepository('Login\Entity\Whitelist')->findAll();
            $whitelistArray = array();
            foreach ($whitelistList as $whitelist) {
                $module = $whitelist->getControllerAction()->getController()->getModule()->getNome();
                $controller = $whitelist->getControllerAction()->getController()->getNome();
                $action = $whitelist->getControllerAction()->getAction()->getNome();
                array_push($whitelistArray, ucfirst($module) . '\\' . ucfirst($controller) . '\\' . ucfirst($action));
            }
            $this->storage->write($whitelistArray);
        }
        return $this->storage->read();
    }
}
