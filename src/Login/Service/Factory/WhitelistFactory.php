<?php
namespace Login\Service\Factory;

use Interop\Container\ContainerInterface;
use Login\Service\Whitelist;
use Zend\Session\SessionManager;
use Zend\Authentication\Storage\Session as SessionStorage;

/**
 * This is the factory class for UserManager service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class WhitelistFactory
{
    /**
     * This method creates the UserManager service and returns its instance. 
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $sessionManager = $container->get(SessionManager::class);
        $whitelistStorage = new SessionStorage('Whitelist', 'session', $sessionManager);

        return new Whitelist($entityManager, $whitelistStorage);
    }
}
