<?php
namespace Login\Service\Factory;

use Interop\Container\ContainerInterface;
use Login\Service\Acl;
use Zend\Session\SessionManager;
/**
 * This is the factory class for UserManager service. The purpose of the factory
 * is to instantiate the service and pass it dependencies (inject dependencies).
 */
class AclFactory
{
    /**
     * This method creates the UserManager service and returns its instance. 
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $sessionManager = $container->get(SessionManager::class);
        return new Acl($entityManager, $sessionManager);
    }
}
