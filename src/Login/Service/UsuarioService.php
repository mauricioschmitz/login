<?php
/**
 * @uthor Mauricio Schmitz
 */

namespace Login\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;
use Base\Utility\UserPassword;

class UsuarioService extends AbstractService
{
    // Construtor
    public function __construct(EntityManager $entityManager)
    {
        $this->entity = \Login\Entity\Usuario::class;
        parent::__construct($entityManager);
    }

    public function save (Array $data = array(), $flushAoFinalizar = false)
    {
        $userPassword = new UserPassword();
        $data['senha'] = $userPassword->create($data['senha']);
        return parent::save($data, $flushAoFinalizar);
    }

    public function validPassword($senha, $usuario){
        $userPassword = new UserPassword();
        return $userPassword->verify($senha, $usuario->getSenha());
    }

    /**
     * Método abastrato para saber se o usuário pode executar a ação
     * @return mixed
     */
    public function isAllowed($entity){
        return true;
    }
}