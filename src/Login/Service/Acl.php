<?php
namespace Login\Service;

use Doctrine\ORM\EntityManager;
use Zend\Form\Element\DateTime;
use Zend\Permissions\Acl\Acl as ZendAcl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Session\SessionManager;
use Zend\Authentication\Storage\Session as SessionStorage;

class Acl extends ZendAcl
{

    const DEFAULT_ROLE = 'guest';

    protected $roles;

    protected $resources;

    protected $rolePermission;

    protected $usuarioPermission;

    protected $usuarioPermissionDenied;

    protected $commonPermission;

    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Zend\Session session manager.
     * @var Zend\Session\SessionManager
     */
    private $sessionManager;

    /**
     * Constructs the service.
     */
    public function __construct(EntityManager $entityManager, SessionManager $sessionManager)
    {
        $this->entityManager = $entityManager;
        $this->sessionManager = $sessionManager;
    }

    public function initAcl($usuario)
    {
        $aclStorage = new SessionStorage('Acl', 'session', $this->sessionManager);
        if(is_null($aclStorage->read())) {
            $roles = $this->_getAllRoles();
            $resources = $this->_getAllResources();
            $rolePermission = $this->_getRolePermissions();

            $usuarioPermissions = $this->initUsuarioPermission($usuario);

            $acl = array(
                'roles' => $roles,
                'resources' => $resources,
                'rolePermission' => $rolePermission,
                'usuarioPermissions' => $usuarioPermissions,
                'date' => new \DateTime('NOW')
            );

            $aclStorage->write($acl);
        }
        $acl = $aclStorage->read();

        $this->roles = $acl['roles'];
        $this->resources = $acl['resources'];
        $this->rolePermission = $acl['rolePermission'];
        $this->usuarioPermission = $acl['usuarioPermissions'][0];
        $this->usuarioPermissionDenied = $acl['usuarioPermissions'][1];

        // we are not putting these resource & permission in table bcz it is
        // common to all user
        $this->commonPermission = array(

        );
        $this->_addRoles()
            ->_addResources()
            ->_addRoleResources();
    }

    public function initUsuarioPermission($usuario){
        $usuarioPermission = array();
        $usuarioPermissionDenied = array();
        $lista = $this->_getUsuarioPermissions($usuario);
        foreach($lista as $item){
            if($item['negar']){
                array_push($usuarioPermissionDenied, $item['permissao']);
            }else{
                array_push($usuarioPermission, $item['permissao']);

            }
        }

        return array($usuarioPermission, $usuarioPermissionDenied);
    }
    
    public function isAccessAllowed($roles, $resource, $permission)
    {
        if (! $this->hasResource($resource)) {
            return false;
        }
        if(in_array($resource.'\\'.$permission, $this->usuarioPermissionDenied)){
            return false;
        }

        if(in_array($resource.'\\'.$permission, $this->usuarioPermission)){
            return true;
        }
        foreach($roles as $role){
            if($role->getApelido() == 'master'){
                return true;

            }elseif($this->isAllowed($role->getApelido(), $resource, $permission)){
                return true;

            }
        }
        return false;
    }

    protected function _addRoles()
    {
        $this->addRole(new Role(self::DEFAULT_ROLE));
        
        if (! empty($this->roles)) {
            foreach ($this->roles as $role) {
                $roleName = $role['apelido'];
                if (! $this->hasRole($roleName)) {
                    $this->addRole(new Role($roleName), self::DEFAULT_ROLE);
                }
            }
        }
        return $this;
    }

    protected function _addResources()
    {
        if (! empty($this->resources)) {
            foreach ($this->resources as $resource) {
                if (! $this->hasResource($resource['nome'])) {
                    $resource['nome'] = $resource['module']->getNome().'\\'.$resource['nome'];

                    $this->addResource(new Resource($resource['nome']));
                }

            }

        }

        // add common resources
        if (! empty($this->commonPermission)) {
            foreach ($this->commonPermission as $resource => $permissions) {
                if (! $this->hasResource($resource)) {
                    $this->addResource(new Resource($resource));
                }
            }
        }
        
        return $this;
    }

    protected function _addRoleResources()
    {
        // allow common resource/permission to guest user
        if (! empty($this->commonPermission)) {
            foreach ($this->commonPermission as $resource => $permissions) {
                foreach ($permissions as $permission) {
                    $this->allow(self::DEFAULT_ROLE, $resource, $permission);
                }
            }
        }
        
        if (! empty($this->rolePermission)) {
            foreach ($this->rolePermission as $rolePermissions) {
                $this->allow($rolePermissions['role'], $rolePermissions['resource'], $rolePermissions['permission']);
            }
        }

        return $this;
    }

    protected function _getAllRoles()
    {
        $lista = $this->entityManager->getRepository('Login\Entity\Perfil')->findByAtivo(true);
        $retorno = array();
        foreach($lista as $perfil){
            array_push($retorno, $perfil->toArray());
        }
        return $retorno;
    }

    protected function _getAllResources()
    {
        $lista = $this->entityManager->getRepository('Login\Entity\Controller')->findByAtivo(true);
        $retorno = array();
        foreach($lista as $resource){
            array_push($retorno, $resource->toArray());
        }
        return $retorno;
    }

    protected function _getRolePermissions()
    {
        $rolePermissao = $this->entityManager->getRepository('Login\Entity\ControllerAction')->getPerfilControllerAction();
        return $rolePermissao;
    }


    protected function _getUsuarioPermissions($usuario)
    {
        $usuarioPermissao = $this->entityManager->getRepository('Login\Entity\UsuarioPermissao')->getPermissaoControllerAction($usuario);
        return $usuarioPermissao;
    }
    
    private function debugAcl($role, $resource, $permission)
    {
        echo 'Role:-' . $role . '==>' . $resource . '\\' . $permission . '<br/>';
    }


}
