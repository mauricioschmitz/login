<?php
/**
 * @uthor Mauricio Schmitz
 */

namespace Login\Service;

use Doctrine\ORM\EntityManager;
use Base\Service\AbstractService;

class UsuarioPerfilService extends AbstractService
{
    public function __construct(EntityManager $entityManager)
    {
        $this->entity = \Login\Entity\UsuarioPerfil::class;
        parent::__construct($entityManager);
    }

    /**
     * Método abastrato para saber se o usuário pode executar a ação
     * @return mixed
     */
    public function isAllowed($entity){
        return true;
    }
}