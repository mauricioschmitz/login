<?php
/**
 * @uthor Mauricio Schmitz
 */

namespace Login\Service;

use Base\Service\AbstractService;
use Doctrine\ORM\EntityManager;

class PerfilService extends AbstractService
{
    public function __construct (EntityManager $entityManager)
    {
        $this->entity = \Login\Entity\Perfil::class;
        parent::__construct($entityManager);
    }

    /**
     * Método abastrato para saber se o usuário pode executar a ação
     * @return mixed
     */
    public function isAllowed($entity){
        return true;
    }
}