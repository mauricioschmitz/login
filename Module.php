<?php
/**
 */

namespace Login;

use Login\Entity\Whitelist;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\Session\SessionManager;
use Zend\Authentication\Storage\Session as SessionStorage;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * This method is called once the MVC bootstrapping is complete and allows
     * to register event listeners.
     */
    public function onBootstrap(MvcEvent $event)
    {
        // Get event manager.
        $eventManager = $event->getApplication()->getEventManager();
        $sharedEventManager = $eventManager->getSharedManager();
        // Register the event listener method.
        $sharedEventManager->attach(AbstractActionController::class,
            MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], 100);
    }

    public function onDispatch(MvcEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();
        $target = $event->getTarget();
        $eventManager = $event->getApplication()->getEventManager();


        $view = $event->getViewModel();

        $serviceManager = $event->getApplication()->getServiceManager();
        $sessionManager = $serviceManager->get(SessionManager::class);

        $authenticationService = $serviceManager->get('Zend\Authentication\AuthenticationService');

        $whitelist = $serviceManager->get('Login\Service\Whitelist');
        $whitelistArray = $whitelist->getWhitelist();

        $module = $event->getRouteMatch()->getParam('module');
        $controller = $event->getRouteMatch()->getParam('controller');
        $action = $event->getRouteMatch()->getParam('action');

        $requestedResourse = ucfirst($module).'\\'.ucfirst($controller) . "\\" . ucfirst($action);

//      Cria uma sessão de debug para o local
        if(isset($serviceManager->get('Config')['local_debug']))
            $_SESSION['Debug_Local'] = $serviceManager->get('Config')['local_debug'];

        if($requestedResourse == 'Login\Auth\Login' && $authenticationService->hasIdentity()){
            $url = $event->getRouter()->assemble(array(), array('name' => 'admin'));
            $response->setHeaders($response->getHeaders()
                ->addHeaderLine('Location', $url));
            $response->setStatusCode(302);
        }
        $view->params = $event->getRouteMatch()->getParams();

        if(!in_array($requestedResourse, $whitelistArray)){ //Se não tiver na whitelist

            if ($authenticationService->hasIdentity()) {

                $loggedUser = $authenticationService->getIdentity();

                $entityManager = $serviceManager->get('doctrine.entitymanager.orm_default');
                $user = $entityManager->getRepository('Login\Entity\Usuario')->find($loggedUser->getId());
                $view->usuarioLogado = $user;

                $perfis = $user->getPerfis();
                $acl = $serviceManager->get('Login\Service\Acl');
                $acl->initAcl($user);

                $status = $acl->isAccessAllowed($perfis, ucfirst($module).'\\'.ucfirst($controller), $action);

                if (! $status) {
                    $url = $event->getRouter()->assemble(array(), array('name' => 'login'));
                    $response->setHeaders($response->getHeaders()
                        ->addHeaderLine('Location', $url));
                    $response->setStatusCode(302);
                }

            }else{//Se não tiver logado
                $url = $event->getRouter()->assemble(array(), array('name' => 'login'));
                $response->setHeaders($response->getHeaders()
                    ->addHeaderLine('Location', $url));

                $response->setStatusCode(302);
                return;
            }
        }
    }
}
