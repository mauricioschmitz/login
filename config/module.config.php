<?php

/**
 * @author mauricioschmitz
 */

namespace Login;

use Zend\Router\Http\Literal;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Session\Storage\SessionArrayStorage;
use Zend\Session\Validator\RemoteAddr;
use Zend\Session\Validator\HttpUserAgent;

return [
    'router' => [
        'routes' => [
            'login' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/login',
                    'defaults' => [
                        'module' => 'login',
                        'controller' => 'Auth',
                        'action' => 'login',
                    ],
                ],
            ],
            'loginexternal' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/loginexternal',
                    'defaults' => [
                        'module' => 'login',
                        'controller' => 'Auth',
                        'action' => 'loginexternal',
                    ],
                ],
            ],
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/logout',
                    'defaults' => [
                        'module' => 'login',
                        'controller' => 'Auth',
                        'action' => 'logout',
                    ],
                ],
            ],
            'forgot' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/forgot',
                    'defaults' => [
                        'module' => 'login',
                        'controller' => 'Auth',
                        'action' => 'forgot',
                    ],
                ],
            ],
            'recover' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/recover',
                    'defaults' => [
                        'module' => 'login',
                        'controller' => 'Auth',
                        'action' => 'recover',
                    ],
                ],
            ],
            'cadastro' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/cadastro',
                    'defaults' => [
                        'module' => 'login',
                        'controller' => 'Cadastro',
                        'action' => 'insert',
                    ],
                ],
            ],
            'termos' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/termos',
                    'defaults' => [
                        'module' => 'login',
                        'controller' => 'Cadastro',
                        'action' => 'termos',
                    ],
                ],
            ],
        ],
    ],
    'view_helpers' => [
        'factories' => [
            View\Helper\AclViewRender::class => View\Helper\Factory\AclViewRenderFactory::class,
            View\Helper\AclHelper::class => View\Helper\Factory\AclHelperFactory::class,
        ],
        'aliases' => [
            'AclViewRender' => View\Helper\AclViewRender::class,
            'AclHelper' => View\Helper\AclHelper::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'login/layout/footer' => __DIR__ . '/../view/layout/footer.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],
    'controllers' => [
        'factories' => [
            Controller\AuthController::class => Controller\Factory\AuthControllerFactory::class,
        ],
        'aliases' => [
            'Auth' => \Login\Controller\AuthController::class,
        ],
    ],
    'controller_plugins' => [
        'factories' => [
            Controller\Plugin\AclPlugin::class => Controller\Plugin\Factory\AclPluginFactory::class,
        ],
        'aliases' => [
            'AclHelperPlugin' => Controller\Plugin\AclPlugin::class,
        ],

    ],
    'service_manager' => [
        'factories' => [
            \Zend\Authentication\AuthenticationService::class => Service\Factory\AuthenticationServiceFactory::class,
            Service\AuthAdapter::class => Service\Factory\AuthAdapterFactory::class,
            Service\AuthManager::class => Service\Factory\AuthManagerFactory::class,
            Service\UserManager::class => Service\Factory\UserManagerFactory::class,
            Service\Acl::class => Service\Factory\AclFactory::class,
            Service\Whitelist::class => Service\Factory\WhitelistFactory::class,
        ],
    ],
    'session_config' => [
        'cookie_lifetime' => 60 * 60 * 1, // Session cookie will expire in 1 hour.
        'gc_maxlifetime' => 60 * 60 * 24 * 30, // How long to store session data on server (for 1 month).
    ],
    // Session manager configuration.
    'session_manager' => [
        // Session validators (used for security).
        'validators' => [
//            RemoteAddr::class,
//            HttpUserAgent::class,
        ]
    ],
    // Session storage configuration.
    'session_storage' => [
        'type' => SessionArrayStorage::class
    ],

];
